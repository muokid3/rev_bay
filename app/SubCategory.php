<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public function getSlugAttribute()
    {
        return str_slug($this->name);
    }

    public function getUrlAttribute()
    {
        return action('CategoryController@subcategory', [$this->id, $this->slug]);
    }
}
