<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\SubCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function category($id, $slug = '')
    {

        $category = Category::find($id);
        $sub_categories = SubCategory::where('category_id', $id)->paginate(20);

        if (is_null($category)){
            return redirect('/');
        } else{
            return view('category')->with(compact('category', 'sub_categories'));
        }

    }

    public function subcategory($id, $slug = '')
    {

        $subcategory = SubCategory::find($id);
        $posts = Post::where('sub_category_id', $id)->paginate(15);

        if (is_null($subcategory)){
            return redirect('/');
        } else{
            return view('subcategory')->with(compact('subcategory', 'posts'));
        }

    }
}
