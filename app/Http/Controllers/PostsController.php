<?php

namespace App\Http\Controllers;

use App\Item;
use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function post($id, $slug = '')
    {

        $post = Post::find($id);
        $items = Item::where('post_id', $id)->paginate(10);

        if (is_null($post)){
            return redirect('/');
        } else{
            return view('post')->with(compact('post','items'));
        }

    }
}
