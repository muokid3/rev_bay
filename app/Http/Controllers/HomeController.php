<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }


    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->paginate(20);
        $recentPosts = Post::orderBy('id', 'desc')->limit(4)->get();
        $latest = Post::orderBy('id', 'desc')->limit(1)->first();

        return view('home')->with(compact('categories','recentPosts', 'latest'));
    }

    public function article()
    {
        return view('post');
    }
}
