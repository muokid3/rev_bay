<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    public function getSlugAttribute()
    {
        return str_slug($this->title);
    }

    public function getUrlAttribute()
    {
        return action('PostsController@post', [$this->id, $this->slug]);
    }
}
