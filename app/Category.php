<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function getSlugAttribute()
    {
        return str_slug($this->name);
    }

    public function getUrlAttribute()
    {
        return action('CategoryController@category', [$this->id, $this->slug]);
    }
}
