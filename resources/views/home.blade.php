@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">


            <div class="row margin-bottom-40">

                <div class="col-md-3 col-sm-3">

                    <!-- BEGIN RECENT NEWS -->
                    <h3><u>Recent Reviews</u></h3>
                    <div class="recent-news margin-bottom-10">


                        @foreach($recentPosts as $recentPost)
                            <div class="row margin-bottom-10">
                                <div class="col-md-3">
                                    <img class="img-responsive" alt="" src="{{config('Droids.admin').$recentPost->image}}">
                                </div>
                                <div class="col-md-9 recent-news-inner">
                                    <h3><a  href="{{$recentPost->url}}">{{$recentPost->title}}</a></h3>
                                    <p>
                                        {!! str_limit(strip_tags($recentPost->description), $limit = 100, $end = '...') !!}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- END RECENT NEWS -->

                </div>


                <div class="col-md-6 col-sm-6">
                    <!-- BEGIN CAROUSEL -->
                    <div class="front-carousel margin-bottom-20">
                        <div id="myCarousel" class="carousel slide">
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img alt="{{$latest->title}}" src="{{config('Droids.admin').$latest->image}}" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CAROUSEL -->

                    <h2 class="no-top-space"><a href="{{$latest->url}}">{{$latest->title}}</a></h2>
                    <ul class="blog-info">
                        <li><i class="fa fa-user"></i> By {{\App\User::where('email', $latest->author)->first() ? \App\User::where('email', $latest->author)->first()->name : "N/A"}}</li>
                        <li><i class="fa fa-calendar"></i> {{$latest->created_at->toDayDateTimeString()}}</li>
                        <li><i class="fa fa-tags"></i> {{\App\SubCategory::find($latest->sub_category_id)->name}}</li>
                    </ul>
                    {{ str_limit(strip_tags($latest->description), $limit = 200, $end = '...') }}


                </div>


                <div class="col-md-3 col-sm-3">

                    <!-- BEGIN BLOG TAGS -->
                    <div class="blog-tags margin-bottom-20">
                        <h2>Tags</h2>
                        <ul>
                            @foreach(\App\SubCategory::all() as $subCat)
                                <li><a href="{{$subCat->url}}"><i class="fa fa-tags"></i>{{$subCat->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- END BLOG TAGS -->

                    <!-- BEGIN RECENT NEWS -->
                    {{--<h3><u>Deals we love <i class="fa fa-heart" style="color:#67bd3c"></i> </u></h3>--}}
                    {{--<div class="recent-news margin-bottom-10">--}}
                        {{--<div class="row margin-bottom-10">--}}
                            {{--<div class="col-md-3">--}}
                                {{--<img class="img-responsive" alt="" src="{{'img/personal-sound-amplification.jpg'}}">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-9 recent-news-inner">--}}
                                {{--<h3><a href="#">The Best Personal Sound Amplification Product</a></h3>--}}
                                {{--<p>--}}
                                    {{--In 50 hours of research and testing, we’ve found that the Sound World Solutions CS50+...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row margin-bottom-10">--}}
                            {{--<div class="col-md-3">--}}
                                {{--<img class="img-responsive" alt="" src="{{'img/personal-sound-amplification.jpg'}}">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-9 recent-news-inner">--}}
                                {{--<h3><a href="#">The Best Personal Sound Amplification Product</a></h3>--}}
                                {{--<p>--}}
                                    {{--In 50 hours of research and testing, we’ve found that the Sound World Solutions CS50+...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row margin-bottom-10">--}}
                            {{--<div class="col-md-3">--}}
                                {{--<img class="img-responsive" alt="" src="{{'img/personal-sound-amplification.jpg'}}">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-9 recent-news-inner">--}}
                                {{--<h3><a href="#">The Best Personal Sound Amplification Product</a></h3>--}}
                                {{--<p>--}}
                                    {{--In 50 hours of research and testing, we’ve found that the Sound World Solutions CS50+...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <!-- END RECENT NEWS -->

                </div>
            </div>


            <!-- BEGIN BLOCKQUOTE BLOCK -->
            <div class="row quote-v1 margin-bottom-30">
                <div class="col-md-12">
                    <span>The Dealersbay - We support our readers with hours of research to help you find the best gadgets/items to improve your life and make shopping easy. You support us through independently chosen links that earn us a commission</span>
                </div>
                {{--<div class="col-md-3 text-right">--}}
                    {{--<a class="btn-transparent" href="#"><i class="fa fa-rocket margin-right-10"></i>Get Started</a>--}}
                {{--</div>--}}
            </div>
            <!-- END BLOCKQUOTE BLOCK -->

            @foreach($categories as $category)
                @if(($category->id % 2) == 1)
                    <!-- BEGIN SERVICES -->
                        <div class="row front-team">
                            <h2 class="no-top-space text-left"><a href="{{$category->url}}">{{$category->name}} </a></h2>
                            <h4 class="no-top-space text-left">
                                @foreach(\App\SubCategory::where('category_id', $category->id)->get() as $subcategory)
                                    <a style="text-decoration: underline" href="{{$subcategory->url}}">{{$subcategory->name}}</a> ,
                                @endforeach
                            </h4>


                            <ul class="list-unstyled">

                                @foreach(\App\Post::whereIn('sub_category_id', \App\SubCategory::select('id')->where('category_id', $category->id)->get())->limit(3)->get() as $topPosts)
                                <li class="col-md-4">
                                        <img alt="{{$topPosts->title}}" src="{{config('Droids.admin').$topPosts->image}}" width="270" height="230"  >
                                        <h4>
                                            <a href="{{$topPosts->url}}"> <strong>{{$topPosts->title}}</strong> </a>
                                        </h4>

                                        <p>
                                            {{ str_limit(strip_tags($topPosts->description), $limit = 200, $end = '...') }}
                                        </p>
                                        <ul class="blog-info">
                                            <li><i class="fa fa-user"></i> By {{\App\User::where('email', $topPosts->author)->first()->name}}</li>
                                            <li><i class="fa fa-calendar"></i> {{$topPosts->created_at->toDayDateTimeString()}}</li>
                                            {{--<li><i class="fa fa-comments"></i> 17</li>--}}
                                            {{--<li><i class="fa fa-tags"></i> Tag1, tag2, tag3</li>--}}
                                        </ul>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                        <!-- END SERVICES -->
                        <hr style="height:1px;border:none;color:#333;background-color:#333;">
                @else
                    <!-- BEGIN RECENT WORKS -->
                        <div class="row recent-work margin-bottom-40">
                            <div class="col-md-3">
                                <h2><a href="{{$category->url}}">{{$category->name}}</a></h2>
                                <p class="no-top-space text-left">
                                    @foreach(\App\SubCategory::where('category_id', $category->id)->get() as $subcategory)
                                        <a style="text-decoration: underline" href="{{$subcategory->url}}">{{$subcategory->name}}</a> ,
                                     @endforeach
                                <br>
                                <br>

                                {!! str_limit(strip_tags($category->description), $limit = 200, $end = '...') !!}


                            </div>
                            <div class="col-md-9">
                                <div class="owl-carousel owl-carousel3">


                                    @foreach(\App\Post::whereIn('sub_category_id', \App\SubCategory::select('id')->where('category_id', $category->id)->get())->limit(3)->get() as $topPosts)
                                        <div class="recent-work-item ">
                                            <em>
                                                <img alt="{{$topPosts->title}}" src="{{config('Droids.admin').$topPosts->image}}" width="270" height="230"  >

                                                <a href="{{$topPosts->url}}"><i class="fa fa-link"></i></a>
                                                <a href=" {{config('Droids.admin').$topPosts->image}}" class="fancybox-button" title="{{$topPosts->title}}" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                                            </em>
                                            <a class="recent-work-description" href="{{$topPosts->url}}">
                                                <strong>{{$topPosts->title}}</strong>

                                                {{ str_limit(strip_tags($topPosts->description), $limit = 100, $end = '...') }}
                                            </a>
                                        </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                        <!-- END RECENT WORKS -->

                        <hr style="height:1px;border:none;color:#333;background-color:#333;">
                @endif

            @endforeach


            <!-- BEGIN STEPS -->
            <div class="row margin-bottom-40 front-steps-wrapper front-steps-count-3">
                <div class="col-md-4 col-sm-4 front-step-col">
                    <div class="front-step front-step1">
                        <h2>Review</h2>
                        <p>Gain insights of your favorite products before purchasing </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 front-step-col">
                    <div class="front-step front-step2">
                        <h2>Decide</h2>
                        <p>Use review knowledge to decide on what brand/type to buy.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 front-step-col">
                    <div class="front-step front-step3">
                        <h2>Purchase</h2>
                        <p>
                            Purchase from a variety of different stores
                        </p>
                    </div>
                </div>
            </div>
            <!-- END STEPS -->


        </div>
    </div>
@endsection