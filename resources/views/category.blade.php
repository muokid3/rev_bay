@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">


            <!-- BEGIN SIDEBAR & CONTENT -->
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12 margin-top-10">
                    <h1>{{$category->name}}</h1>
                    <div class="content-page">
                        <div class="row">
                            <!-- BEGIN LEFT SIDEBAR -->
                            <div class="col-md-9 col-sm-9 blog-item">

                               {!! $category->description !!}

                                 {{--<ul class="blog-info">--}}
                                    {{--<li><i class="fa fa-user"></i> By {{\App\User::where('email', $post->author)->first() ? \App\User::where('email', $post->author)->first()->name : "N/A"}}</li>--}}
                                    {{--<li><i class="fa fa-calendar"></i> {{$post->created_at->toDayDateTimeString()}}</li>--}}
                                    {{--<li><i class="fa fa-tags"></i> {{\App\SubCategory::find($post->sub_category_id)->name}}</li>--}}
                                {{--</ul>--}}

                                <hr>


                                @foreach($sub_categories as $sub_category)
                                    <!-- BEGIN SERVICES -->
                                    <div class="row front-team">
                                        <h3 class="no-top-space text-left"><a href="{{$sub_category->url}}">{{$sub_category->name}} </a></h3>
                                        <ul class="list-unstyled">

                                            @foreach(\App\Post::where('sub_category_id', $sub_category->id)->limit(3)->get() as $topPosts)
                                                <li class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                                                    <img alt="{{$topPosts->title}}" src="{{config('Droids.admin').$topPosts->image}}" width="270" height="230"  >
                                                    <h4>
                                                        <a href="{{$topPosts->url}}"> <strong>{{$topPosts->title}}</strong> </a>
                                                    </h4>

                                                    <p>
                                                        {{ str_limit(strip_tags($topPosts->description), $limit = 100, $end = '...') }}
                                                    </p>
                                                    <ul class="blog-info">
                                                        <li><i class="fa fa-user"></i> By {{\App\User::where('email', $topPosts->author)->first()->name}}</li>
                                                        <br>
                                                        <li><i class="fa fa-calendar"></i> {{$topPosts->created_at->toDayDateTimeString()}}</li>
                                                        {{--<li><i class="fa fa-comments"></i> 17</li>--}}
                                                        {{--<li><i class="fa fa-tags"></i> Tag1, tag2, tag3</li>--}}
                                                    </ul>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                    <!-- END SERVICES -->
                                    <hr style="height:1px;border:none;color:#333;background-color:#333;">
                                @endforeach




                            </div>
                            <!-- END LEFT SIDEBAR -->

                            <!-- BEGIN RIGHT SIDEBAR -->
                            <div class="col-md-3 col-sm-3 blog-sidebar">
                                <!-- CATEGORIES START -->
                                <h2 class="no-top-space">Top Categories</h2>
                                <ul class="nav sidebar-categories margin-bottom-40">
                                    @foreach(\App\Category::take(5)->get() as $category)
                                        <li><a href="{{$category->url}}">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                                <!-- CATEGORIES END -->

                                <!-- BEGIN RECENT NEWS -->
                                <h2>Recent News</h2>
                                <div class="recent-news margin-bottom-10">
                                    <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                            <img class="img-responsive" alt="" src="assets/pages/img/people/img2-large.jpg">
                                        </div>
                                        <div class="col-md-9 recent-news-inner">
                                            <h3><a href="javascript:;">Letiusto gnissimos</a></h3>
                                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                                        </div>
                                    </div>
                                    <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                            <img class="img-responsive" alt="" src="assets/pages/img/people/img1-large.jpg">
                                        </div>
                                        <div class="col-md-9 recent-news-inner">
                                            <h3><a href="javascript:;">Deiusto anissimos</a></h3>
                                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                                        </div>
                                    </div>
                                    <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                            <img class="img-responsive" alt="" src="assets/pages/img/people/img3-large.jpg">
                                        </div>
                                        <div class="col-md-9 recent-news-inner">
                                            <h3><a href="javascript:;">Tesiusto baissimos</a></h3>
                                            <p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- END RECENT NEWS -->

                                <!-- BEGIN BLOG TALKS -->
                                <div class="blog-talks margin-bottom-30">
                                    <h2>Popular Talks</h2>
                                    <div class="tab-style-1">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#tab-1">Multipurpose</a></li>
                                            <li><a data-toggle="tab" href="#tab-2">Documented</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-1" class="tab-pane row-fluid fade in active">
                                                <p class="margin-bottom-10">Raw denim you probably haven't heard of them jean shorts Austin. eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p>
                                                <p><a class="more" href="javascript:;">Read more</a></p>
                                            </div>
                                            <div id="tab-2" class="tab-pane fade">
                                                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. aliquip jean shorts ullamco ad vinyl aesthetic magna delectus mollit. Keytar helvetica VHS salvia..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BLOG TALKS -->


                                <!-- BEGIN BLOG TAGS -->
                                <div class="blog-tags margin-bottom-20">
                                    <h2>Tags</h2>
                                    <ul>
                                        @foreach(\App\SubCategory::all() as $subCat)
                                            <li><a href="{{$subCat->url}}"><i class="fa fa-tags"></i>{{$subCat->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- END BLOG TAGS -->
                            </div>
                            <!-- END RIGHT SIDEBAR -->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END SIDEBAR & CONTENT -->


        </div>
    </div>
@endsection