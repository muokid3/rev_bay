@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">


            <!-- BEGIN SIDEBAR & CONTENT -->
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <h1>{{$post->title}}</h1>
                    <div class="content-page">
                        <div class="row">
                            <!-- BEGIN LEFT SIDEBAR -->
                            <div class="col-md-9 col-sm-9 blog-item">
                                <div class="blog-item-img">
                                    <!-- BEGIN CAROUSEL -->
                                    <div class="front-carousel">
                                        <div id="myCarousel" class="carousel slide">
                                            <!-- Carousel items -->
                                            <div class="carousel-inner">
                                                <div class="item active">
                                                    <img alt="{{$post->title}}" src="{{config('Droids.admin').$post->image}}">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- END CAROUSEL -->
                                </div>
                               {!! $post->description !!}

                                 <ul class="blog-info">
                                    <li><i class="fa fa-user"></i> By {{\App\User::where('email', $post->author)->first() ? \App\User::where('email', $post->author)->first()->name : "N/A"}}</li>
                                    <li><i class="fa fa-calendar"></i> {{$post->created_at->toDayDateTimeString()}}</li>
                                    {{--<li><i class="fa fa-comments"></i> 17</li>--}}
                                    <li><i class="fa fa-tags"></i> {{\App\SubCategory::find($post->sub_category_id)->name}}</li>
                                </ul>

                                <h2>Product Reviews</h2>
                                <hr>

                                @foreach($items as $item)

                                <div class="col-md-12 col-sm-12">
                                    <div class="content-page">
                                        <div class="row margin-bottom-30">
                                            <!-- BEGIN CAROUSEL -->
                                            <div class="col-md-5 front-carousel">
                                                <div class="carousel slide" id="myCarousel">
                                                    <!-- Carousel items -->
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <img alt="{{$item->title}}" src="{{config('Droids.admin').$item->image}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END CAROUSEL -->

                                            <!-- BEGIN PORTFOLIO DESCRIPTION -->
                                            <div class="col-md-7">
                                                <h2 style="color: red">{{$item->title}}</h2>
                                                <h3>{{$item->tagline}}</h3>
                                                {!! $item->description !!}
                                                <br>
                                                <br>


                                                @foreach(\App\Price::where('item_id', $item->id)->get() as $price)
                                                    <a class="btn btn-danger" target="_blank" href="{{$price->link}}"> <strong style="color: white">{{$price->currency." ". $price->amount." from ".$price->store }}</strong></a>

                                                @endforeach
                                                <hr>
                                            </div>
                                            <!-- END PORTFOLIO DESCRIPTION -->
                                        </div>

                                    </div>
                                </div>
                                @endforeach

                                {!! $items->render() !!}


                            </div>
                            <!-- END LEFT SIDEBAR -->

                            <!-- BEGIN RIGHT SIDEBAR -->
                            <div class="col-md-3 col-sm-3 blog-sidebar">
                                <!-- CATEGORIES START -->
                                <h2 class="no-top-space">Top Categories</h2>
                                <ul class="nav sidebar-categories margin-bottom-40">
                                    @foreach(\App\Category::take(5)->get() as $category)
                                        <li><a href="{{$category->url}}">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                                <!-- CATEGORIES END -->

                                <!-- BEGIN RECENT NEWS -->
                                {{--<h2>Recent News</h2>--}}
                                {{--<div class="recent-news margin-bottom-10">--}}
                                    {{--<div class="row margin-bottom-10">--}}
                                        {{--<div class="col-md-3">--}}
                                            {{--<img class="img-responsive" alt="" src="">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 recent-news-inner">--}}
                                            {{--<h3><a href="javascript:;">Letiusto gnissimos</a></h3>--}}
                                            {{--<p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row margin-bottom-10">--}}
                                        {{--<div class="col-md-3">--}}
                                            {{--<img class="img-responsive" alt="" src="">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 recent-news-inner">--}}
                                            {{--<h3><a href="javascript:;">Deiusto anissimos</a></h3>--}}
                                            {{--<p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row margin-bottom-10">--}}
                                        {{--<div class="col-md-3">--}}
                                            {{--<img class="img-responsive" alt="" src="">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 recent-news-inner">--}}
                                            {{--<h3><a href="javascript:;">Tesiusto baissimos</a></h3>--}}
                                            {{--<p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <!-- END RECENT NEWS -->

                                {{--<!-- BEGIN BLOG TALKS -->--}}
                                {{--<div class="blog-talks margin-bottom-30">--}}
                                    {{--<h2>Popular Talks</h2>--}}
                                    {{--<div class="tab-style-1">--}}
                                        {{--<ul class="nav nav-tabs">--}}
                                            {{--<li class="active"><a data-toggle="tab" href="#tab-1">Multipurpose</a></li>--}}
                                            {{--<li><a data-toggle="tab" href="#tab-2">Documented</a></li>--}}
                                        {{--</ul>--}}
                                        {{--<div class="tab-content">--}}
                                            {{--<div id="tab-1" class="tab-pane row-fluid fade in active">--}}
                                                {{--<p class="margin-bottom-10">Raw denim you probably haven't heard of them jean shorts Austin. eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p>--}}
                                                {{--<p><a class="more" href="javascript:;">Read more</a></p>--}}
                                            {{--</div>--}}
                                            {{--<div id="tab-2" class="tab-pane fade">--}}
                                                {{--<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. aliquip jean shorts ullamco ad vinyl aesthetic magna delectus mollit. Keytar helvetica VHS salvia..</p>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <!-- END BLOG TALKS -->


                                <!-- BEGIN BLOG TAGS -->
                                <div class="blog-tags margin-bottom-20">
                                    <h2>Tags</h2>
                                    <ul>
                                        @foreach(\App\SubCategory::all() as $subCat)
                                            <li><a href="{{$subCat->url}}"><i class="fa fa-tags"></i>{{$subCat->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- END BLOG TAGS -->
                            </div>
                            <!-- END RIGHT SIDEBAR -->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END SIDEBAR & CONTENT -->


        </div>
    </div>
@endsection