
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>The Dealer's Bay</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <!-- Fonts START -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="{{url('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Global styles END -->


    <!-- Page level plugin styles START -->
    <link href="{{url('assets/pages/css/animate.css" rel="stylesheet')}}">
    <link href="{{url('assets/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{url('assets/plugins/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
    <!-- Page level plugin styles END -->

    <!-- Theme styles START -->
    <link href="{{url('assets/pages/css/components.css')}}" rel="stylesheet">
    <link href="{{url('assets/pages/css/slider.css')}}" rel="stylesheet">
    <link href="{{url('assets/corporate/css/style.css')}}" rel="stylesheet">
    <link href="{{url('assets/corporate/css/style-responsive.css')}}" rel="stylesheet">
    <link href="{{url('assets/corporate/css/themes/green.css')}}" rel="stylesheet" id="style-color">
    <link href="{{url('assets/corporate/css/custom.css')}}" rel="stylesheet">
    <!-- Theme styles END -->

    @yield('styles')

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <a class="site-logo" href="{{url('/')}}"><img src="{{url('img/logo.png')}}"  alt="JS Pinnacle"></a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
            <ul>

                @foreach(\App\Category::limit(5)->get() as $category)
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        {{$category->name}}

                    </a>

                    <ul class="dropdown-menu">
                        <li><a href="{{$category->url}}"><strong>All {{$category->name}}</strong></a></li>

                        @foreach(\App\SubCategory::where('category_id', $category->id)->get() as $subCategory)
                            <li><a href="{{$subCategory->url}}">{{$subCategory->name}}</a></li>
                        @endforeach

                    </ul>
                </li>

                @endforeach

                <!-- BEGIN TOP SEARCH -->

                    <li  class="menu">
                        <a class="dropdown-toggle" target="_blank" href="{{url('TERMS_OF_USE.pdf')}}">
                            Terms

                        </a>
                    </li>
                {{--<li class="menu-search">--}}
                    {{--<span class="sep"></span>--}}
                    {{--<i class="fa fa-search search-btn"></i>--}}
                    {{--<div class="search-box">--}}
                        {{--<form action="#">--}}
                            {{--<div class="input-group">--}}
                                {{--<input type="text" placeholder="Search" class="form-control">--}}
                                {{--<span class="input-group-btn">--}}
                      {{--<button class="btn btn-primary" type="submit">Search</button>--}}
                    {{--</span>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</li>--}}
                <!-- END TOP SEARCH -->
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
<!-- Header END -->


@yield('content')


<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-4 padding-top-10">
                © 2018 The Dealer's Bay.

                <br>

                <a style="text-decoration: none; color: white" href="{{url('TERMS_OF_USE.pdf')}}" target="_blank">
                    Terms of Use
                </a>

                <br>

                <a style="text-decoration: none; color: white" href="{{url('PRIVACY_POLICY.pdf')}}" target="_blank">
                    Privacy Policy
                </a>


            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN PAYMENTS -->
            <div class="col-md-4 col-sm-4">
                Kenyatta Street <br> Rev Building – 1st Floor <br> <p class="powered">All rights reserved</p>

            </div>
            <!-- END PAYMENTS -->
            <!-- BEGIN POWERED -->
            <div class="col-md-2 col-sm-4">
                <a style="text-decoration: none" href="#" target="_blank">
                    <i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>  &nbsp; Facebook
                </a>

                <br>

                <a style="text-decoration: none" href="#" target="_blank">
                    <i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i> &nbsp; Follow us on Twitter
                </a>

                <br>

                <a style="text-decoration: none" href="#" target="_blank">
                    <i class="fa fa-instagram fa-2x" aria-hidden="true"></i> &nbsp; Instagram
                </a>
                <br>
            </div>
            <!-- END POWERED -->
        </div>
    </div>
</div>
<!-- END FOOTER -->

<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="{{url('assets/plugins/respond.min.js')}}" type="text/javascript"></script>
<![endif]-->
<script src="{{url('assets/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/corporate/scripts/back-to-top.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{url('assets/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{url('assets/plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->

<script src="{{url('assets/corporate/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{url('assets/pages/scripts/bs-carousel.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        Layout.initTwitter();
        Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
        Layout.initNavScrolling();
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->

@yield('scripts')

</body>
<!-- END BODY -->
</html>
