<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');


Route::get('/home', 'HomeController@index');
Route::get('/article', 'HomeController@article');
Route::get('/post/{post_id}/{slug?}', 'PostsController@post');
Route::get('/category/{category_id}/{slug?}', 'CategoryController@category');
Route::get('/subcategory/{subcategory_id}/{slug?}', 'CategoryController@subcategory');
